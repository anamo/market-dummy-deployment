<?php /*! anamo/market-dummy-deployment v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/market-dummy-deployment */

set_error_handler(function ($errno, $errstr, $errfile, $errline) {
	file_get_contents('https://api.telegram.org/bot178153406:AAEmBXf4gjvO577bp25eBXKhDCPnKyBJ0Iw/sendMessage?chat_id=178163713&text='.urlencode(php_uname('n').';anamo/market-dummy-deployment '.$errfile.'@'.$errline.': '.$errstr));
	http_response_code(E_USER_ERROR == $errno ? $errstr : 500);
	echo (string) (include stream_resolve_include_path('build/error.php'))([]);
	return false; // NOTE: FALSE = Don't execute PHP internal error handler
}, E_ERROR | E_PARSE | E_USER_ERROR);

set_exception_handler(function (Throwable $ex) {
	file_get_contents('https://api.telegram.org/bot178153406:AAEmBXf4gjvO577bp25eBXKhDCPnKyBJ0Iw/sendMessage?chat_id=178163713&text='.urlencode(php_uname('n').';anamo/cloud-commerce-director '.$ex->getFile().'@'.$ex->getLine().': '.$ex->getMessage()));
	trigger_error(500, E_USER_ERROR);
});

if (!class_exists('\Router')) {
	final class Router extends Dummy\Router
	{}
}

if (!class_exists('\Webstore')) {
	final class Webstore extends Dummy\Webstore
	{}
}

$Router = Router::singleton();
$Router->route();
