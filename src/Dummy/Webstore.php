<?php /*! anamo/market-dummy-deployment v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/market-dummy-deployment */

namespace Dummy;

class Webstore
{
	protected static $categories = null;
	protected static $checkoutZone = null;
	protected static $manufacturers = null;
	protected static $instance = null;
	protected static $metas = [];
	protected static $model = null;
	protected static $pixel = null;

	protected static $pageview_batch = [];

	public static function init()
	{
		self::$instance = new \Market\Webstore([
			'app_id' => MANIFEST[\Market\Webstore::APP_ID_ENV_NAME],
			'app_secret' => MANIFEST[\Market\Webstore::APP_SECRET_ENV_NAME],
			'default_olympian_version' => MANIFEST[\Market\Webstore::OLYMPIAN_VERSION_ENV_NAME],
			'enable_beta_mode' => true == MANIFEST['beta_mode'],
			'override_olympian_host' => getenv('OLYMPIAN_HOST')
		]);

		// pixel
		self::$pixel = '';

		// metas
		// canonical metas
		array_push(
			self::$metas,
			'<link rel="canonical" href="'.($_SERVER['HTTP_X_FORWARDED_PROTO'] ?? $_SERVER['REQUEST_SCHEME']).'://'.($_SERVER['HTTP_X_FORWARDED_HOST'] ?? $_SERVER['SERVER_NAME']).$_SERVER['APP_SELF'].(array_key_exists('APP_PATH_SUFFIX', $_SERVER) ? (mb_substr('/' == $_SERVER['APP_SELF'], -1) ? mb_substr($_SERVER['APP_PATH_SUFFIX'], 1) : $_SERVER['APP_PATH_SUFFIX']) : '').'">'
		);
		if (!empty(MANIFEST['locales'])) {
			foreach (array_map(fn($v) => str_replace('_', '-', trim(reset(explode('@', locale_canonicalize($v), 2)))), MANIFEST['locales']) as $k => $v) {
				array_push(
					self::$metas,
					'<link rel="alternate" href="'.($_SERVER['HTTP_X_FORWARDED_PROTO'] ?? $_SERVER['REQUEST_SCHEME']).'://'.($_SERVER['HTTP_X_FORWARDED_HOST'] ?? $_SERVER['SERVER_NAME']).$_SERVER['APP_SELF'].(mb_substr($_SERVER['APP_SELF'], -1) != '/' ? '/'.$k : $k).'" hreflang="'.$v.'">'
				);
			}
		}
		// cart metas
		array_push(
			self::$metas,
			'<link rel="dns-prefetch" href="//anamo.solutions">',
			'<link rel="prerender" href="/cart/summary'.$_SERVER['APP_PATH_SUFFIX'].'">'
		);
		// rest metas
		array_push(
			self::$metas,
			'<meta http-equiv="Content-Script-Type" content="text/javascript">',
			'<meta http-equiv="Content-Style-Type" content="text/css">'
		);
	}

	public static function mount(array &$batch): void
	{
		$batch['router-webstore'] = self::$instance->request('GET', '/webstores/'.MANIFEST['webstore_id'].'?'.http_build_query([
			'include' => 'product-design'
		]), [], null, 'etag_by_memcache');
		$batch['router-browsenodes'] = self::$instance->request('GET', '/browsenodes?'.http_build_query([
			'page' => [
				'size' => 100
			],
			'include' => 'i18ns'
		]), [], null, 'etag_by_memcache');
		$batch['router-checkout-zone'] = self::$instance->request('GET', '/checkout-zones?'.http_build_query([
			'page' => [
				'size' => 1
			],
			'include' => 'settings,sales-channels',
			'filter' => [
				'locale' => locale_canonicalize(locale_get_default())
			]
		]), [], null, 'etag_by_memcache');
		$batch['router-manufacturers'] = self::$instance->request('GET', '/filter-items?'.http_build_query([
			'page' => [
				'size' => 100
			],
			'include' => 'i18ns,i18ns.item'
		]), [], null, 'etag_by_memcache');
	}

	public static function unmount(\Market\MarketBatchResponse $batchResponse): void
	{
		$responses = $batchResponse->getResponses();

		if (array_key_exists('router-webstore', $responses)) {
			self::$model = $responses['router-webstore']->getOlympianNode();
		}
		if (array_key_exists('router-browsenodes', $responses)) {
			self::$categories = $responses['router-browsenodes']->getOlympianNodes();
		}
		if (array_key_exists('router-checkout-zone', $responses)) {
			self::$checkoutZone = $responses['router-checkout-zone']->getOlympianNodes();
		}
		if (array_key_exists('router-manufacturers', $responses)) {
			self::$manufacturers = $responses['router-manufacturers']->getOlympianNodes();
		}
	}

	public static function getPageviewsBatch(): array
	{
		return self::$pageview_batch;
	}

	public static function pageviewBatch(?string $query)
	{
		if (empty($query)) {
			return null;
		}
		$key = 'context-hash~'.($cache_key = md5($query));
		if (!array_key_exists($key, self::$pageview_batch)) {
			$etag = null;
			if (!empty($etag_cache_args = json_decode(getenv('PS_WS_ETAG_CACHE_KEYS'), true))) {
				$etag_cache = new \Predis\Client(...$etag_cache_args);
				if ($etag_cache->exists($records_bucket = MANIFEST['webstore_id']."~{$cache_key}")) {
					$etag = $cache_key;
				}
				$etag_cache->disconnect();
			}
			self::$pageview_batch[$key] = self::$instance->request('GET', $query, [], null, $etag);
			return null;
		}
		return self::$pageview_batch[$key];
	}

	public static function unmountPageview(\Market\MarketBatchResponse $batchResponse): void
	{
		$responses = $batchResponse->getResponses();

		foreach (array_filter($responses, fn($k) => 'context-hash~' == mb_substr($k, 0, 13), ARRAY_FILTER_USE_KEY) as $k => $v) {

			$response = $v;

			$squid = md5($v->getRequest()->getEndpoint());

			$etag = $v->getHeaders()['Etag'] ?? null; //$v->getRequest()->getHeaders()['if-none-match'])

			if (!empty($etag_cache_args = json_decode(getenv('PS_WS_ETAG_CACHE_KEYS'), true)) &&
				!empty($etag)) {

				if (304 == $v->getHttpStatusCode()) {

					try {
						$etag_cache = new \Predis\Client(...$etag_cache_args);

						if (($decompressed_body = gzuncompress($etag_cache->get(getenv('WS_DEP_ID')."~cbody:{$squid}")))) {
							$response = new \Market\MarketResponse($v->getRequest(), $decompressed_body, $v->getHttpStatusCode(), $v->getHeaders());

							$etag_cache->expireat(getenv('WS_DEP_ID')."~etag:{$squid}", $expires_at = strtotime('+3 months'));
							$etag_cache->expireat(getenv('WS_DEP_ID')."~cbody:{$squid}", $expires_at);

						} else {
							$etag_cache->del(getenv('WS_DEP_ID')."~etag:{$squid}");
							$etag_cache->del(getenv('WS_DEP_ID')."~cbody:{$squid}");

						}

						$etag_cache->disconnect();

					} catch (\Throwable $e) {}

				} else {

					if ('GET' == $v->getRequest()->getMethod() &&
						0 === strpos($v->getRequest()->getEndpoint(), '/products?')) {

						try {
							$etag_cache = new \Predis\Client(...$etag_cache_args);

							// Prepare body for caching
							$body_to_cache = $v->getDecodedBody();
							unset($body_to_cache['meta']['sql']);

							if (($serialized_body = json_encode($body_to_cache, JSON_UNESCAPED_UNICODE)) &&
								($compressed_body = gzcompress($serialized_body, 4))) {

								$etag_cache->set(getenv('WS_DEP_ID')."~etag:{$squid}", $etag);
								$etag_cache->expireat(getenv('WS_DEP_ID')."~etag:{$squid}", $expires_at = strtotime('+3 months'));

								$etag_cache->set(getenv('WS_DEP_ID')."~cbody:{$squid}", $compressed_body);
								$etag_cache->expireat(getenv('WS_DEP_ID')."~cbody:{$squid}", $expires_at);

							}

							$etag_cache->disconnect();

						} catch (\Throwable $e) {}
					}

				}
			}

			self::$pageview_batch[$k] = $response->getOlympianNodeOrCollection();
		}
	}

	public static function getModel(): ?\Market\OlympianNodes\OlympianNodeWebstore
	{
		return self::$model;
	}

	public static function getCategories(): ?\Market\OlympianNodes\Collection
	{
		return self::$categories;
	}

	public static function getCheckoutZone(): ?\Market\OlympianNodes\OlympianNodeCheckoutZone
	{
		return self::$checkoutZone[0];
	}

	public static function getManufacturers(): ?\Market\OlympianNodes\Collection
	{
		return self::$manufacturers;
	}

	public static function getPixel(): ?string
	{
		return self::$pixel;
	}

	public static function getMetas(): string
	{
		return implode('', self::$metas);
	}

	public static function appendMetas(): string
	{
		return array_push(self::$metas, ...func_get_args());
	}

	public static function __callStatic(string $name, array $arguments)
	{
		if (method_exists(self::$instance, $name)) {
			return call_user_func(array(self::$instance, $name), ...$arguments);
		}
	}
}
