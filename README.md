# Market Dummy Deployment

This is a toolkit for developing Market themes. It&#39;s designed to assist your workflow and speed up the process of developing, testing, and deploying themes.

shell

```shell

$ composer require-dev anamo/market-dummy-deployment

$ composer run-script dummy-deploy-win

$ composer run-script handlebars-win

webpack --config webpack.test.js

```

apache-conf

```apache-conf

Define static_path "/path/to/folder"
Define http_port "port_number"
Define olympian_host "http://localhost:8088"
Include "${static_path}/vendor/anamo/market-dummy-deployment/lib/apache/apache2.conf"
Define content_security_policy "default-src 'self'; script-src 'self' ${http_host}:${http_port} https://connect.facebook.net/ https://www.google.com/recaptcha/ https://www.gstatic.com/recaptcha/ https://chimpstatic.com/; font-src https://anamo.azureedge.net/bootstrap-for-market/; style-src 'self' https://anamo.azureedge.net/bootstrap-for-market/; img-src 'self' https://anamo.blob.core.windows.net/media-test/; frame-src https://www.google.com/; prefetch-src https://anamo.azureedge.net/bootstrap-for-market/;"

```
